﻿namespace TrekkingForCharity.Fenix.Client.TestHarness
{
    using System;
    using Executors.CommandResults.Auth;

    public class DummyAuthorizationProvider : AuthorizationProvider
    {
        public DummyAuthorizationProvider(string apiPath, Guid apiKey) : base(apiPath, apiKey)
        {
        }

        public override string Token { get; set; }
        public override DateTime? ExpiryDate { get; set; }

        public override void ProcessRenewToken(RenewTokenCommandResult renewTokenCommandResult)
        {
            Token = renewTokenCommandResult.AuthToken;
            ExpiryDate = renewTokenCommandResult.ExpireDate;
        }

        public override void ProcessLogin(LoginUserCommandResult loginUserCommandResult)
        {
            Token = loginUserCommandResult.AuthToken;
            ExpiryDate = loginUserCommandResult.ExpireDate;
        }
    }
}
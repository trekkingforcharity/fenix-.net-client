﻿namespace TrekkingForCharity.Fenix.Client.TestHarness
{
    using System;
    using Executors.CommandResults.User;
    using Executors.Commands.User;
    using TrekkingForCharity.Fenix.Client.Queries.Entities.User;

    class Program
    {
        static void Main(string[] args)
        {
            var fenixClient = new FenixClient(new DummyAuthorizationProvider("http://localhost:7490/api/", Guid.Parse("79A75396-CDF0-4BF6-95AE-7A062C553144")));

            fenixClient.NotifyEvent += FenixClient_NotifyEvent;

            var query = fenixClient.CreateQuery<User>();
            query.Filter(x => x.FirstName == "System");
            var res = fenixClient.Query(query).Result;


        }

        private static void FenixClient_NotifyEvent(object sender, Infrastructure.NoticationEventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}

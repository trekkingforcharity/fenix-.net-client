﻿param (
    [string]$user,
    [string]$pass
)

Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

Write-Host $user
Write-Host $pass

$pair = "$($user):$($pass)"

$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))

Write-Host $encodedCreds

$basicAuthValue = "Basic $encodedCreds"

$Headers = @{
    Authorization = $basicAuthValue
}

$response = Invoke-RestMethod -Uri "https://trekkingforcharity.visualstudio.com/DefaultCollection/Fenix/_apis/build/builds?statusFilter=completed&`$top=1&api-version=2.0" -Headers $Headers -Method Get
Write-Host $response
$id = $response.value[0].id
Write-Host $id
$response = Invoke-RestMethod -Uri "https://trekkingforcharity.visualstudio.com/defaultcollection/Fenix/_apis/build/builds/$id/artifacts" -Headers $Headers -Method Get
$downloadUrl = $response.value[0].resource.downloadUrl
Write-Host "Donload url: $downloadUrl"
Invoke-WebRequest -Uri $response.value[0].resource.downloadUrl -Headers $Headers -Method Get -OutFile GenClient.zip

Get-ChildItem .\


Unzip ".\GenClient.zip" ".\"

Get-ChildItem ".\generated-net-client"

Copy-Item ".\generated-net-client\CommandResults.cs" -Destination ".\NetClient\Executors\CommandResults" -Force
Copy-Item ".\generated-net-client\Commands.cs" -Destination ".\NetClient\Executors\Commands" -Force
Copy-Item ".\generated-net-client\GlobalConstants.cs" -Destination ".\NetClient\GlobalConstants" -Force
Copy-Item ".\generated-net-client\QueryHelpers.cs" -Destination ".\NetClient\Helpers" -Force
Copy-Item ".\generated-net-client\Reads.cs" -Destination ".\NetClient\Queries\Entities" -Force


﻿namespace TrekkingForCharity.Fenix.Client.Queries
{
    using System.Collections.Generic;

    public class ApiQueryResponse<T>
    {
        /// <summary>
        ///     Gets or sets a value indicating whether has results.
        /// </summary>
        public bool HasResults { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether more results available.
        /// </summary>
        public bool MoreResultsAvailable { get; set; }

        /// <summary>
        ///     Gets or sets the page.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        ///     Gets or sets the results.
        /// </summary>
        public ICollection<T> Results { get; set; }

        /// <summary>
        ///     Gets or sets the results per page.
        /// </summary>
        public int ResultsPerPage { get; set; }

        /// <summary>
        ///     Gets or sets the total pages.
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        ///     Gets or sets the total results.
        /// </summary>
        public int TotalResults { get; set; }
    }
}
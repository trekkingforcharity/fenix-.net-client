﻿namespace TrekkingForCharity.Fenix.Client.Queries.QueryGeneration
{
    using System;
    using System.Linq.Expressions;
    using TrekkingForCharity.Fenix.Client.Queries.Entities;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TQueryEntity"></typeparam>
    public abstract class BaseODataQuery<TQueryEntity> where TQueryEntity : QueryEntity
    {
        public abstract ODataQuery<TQueryEntity> Filter(Expression<Func<TQueryEntity, bool>> expression);
        public abstract ODataQuery<TQueryEntity> OrderBy(Expression<Func<TQueryEntity, object>> expression);
        public abstract ODataQuery<TQueryEntity> OrderByDescending(Expression<Func<TQueryEntity, object>> expression);
        public abstract ODataQuery<TQueryEntity> Skip(int count);
        public abstract ODataQuery<TQueryEntity> ThenBy(Expression<Func<TQueryEntity, object>> expression);
        public abstract ODataQuery<TQueryEntity> ThenByDescending(Expression<Func<TQueryEntity, object>> expression);
        public abstract ODataQuery<TQueryEntity> Top(int count);
    }
}
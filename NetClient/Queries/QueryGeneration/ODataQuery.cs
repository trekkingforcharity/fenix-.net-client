﻿namespace TrekkingForCharity.Fenix.Client.Queries.QueryGeneration
{
    using System;
    using System.Linq.Expressions;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Simple.OData.Client;
    using TrekkingForCharity.Fenix.Client.Queries.Entities;
    using SimpleClient = Simple.OData.Client.ODataClient;

    public class ODataQuery<TQueryEntity> : BaseODataQuery<TQueryEntity> where TQueryEntity : QueryEntity
    {
        private readonly IBoundClient<TQueryEntity> _boundClient;

        internal ODataQuery(SimpleClient client)
        {
            this._boundClient = client.For<TQueryEntity>();
        }

        public override ODataQuery<TQueryEntity> Filter(Expression<Func<TQueryEntity, bool>> expression)
        {
            this._boundClient.Filter(expression);
            return this;
        }

        public override ODataQuery<TQueryEntity> OrderBy(Expression<Func<TQueryEntity, object>> expression)
        {
            this._boundClient.OrderBy(expression);
            return this;
        }

        public override ODataQuery<TQueryEntity> ThenBy(Expression<Func<TQueryEntity, object>> expression)
        {
            this._boundClient.ThenBy(expression);
            return this;
        }

        public override ODataQuery<TQueryEntity> OrderByDescending(Expression<Func<TQueryEntity, object>> expression)
        {
            this._boundClient.OrderByDescending(expression);
            return this;
        }

        public override ODataQuery<TQueryEntity> ThenByDescending(Expression<Func<TQueryEntity, object>> expression)
        {
            this._boundClient.ThenByDescending(expression);
            return this;
        }

        public override ODataQuery<TQueryEntity> Skip(int count)
        {
            this._boundClient.Skip(count);
            return this;
        }

        public override ODataQuery<TQueryEntity> Top(int count)
        {
            this._boundClient.Top(count);
            return this;
        }

        internal async Task<string> GetCommandTextAsync()
        {
            var commandText = await this._boundClient.GetCommandTextAsync();

            var rgx = new Regex(@"\.");
            return rgx.Replace(commandText, "/", 1);
        }
    }
}
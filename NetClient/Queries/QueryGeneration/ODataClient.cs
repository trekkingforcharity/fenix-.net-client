﻿namespace TrekkingForCharity.Fenix.Client.Queries.QueryGeneration
{
    using System;
    using System.IO;
    using System.Reflection;
    using TrekkingForCharity.Fenix.Client.Queries.Entities;
    using SimpleClient = Simple.OData.Client.ODataClient;
    using SimpleSettings = Simple.OData.Client.ODataClientSettings;

    internal class ODataClient
    {
        private SimpleClient _client;

        internal ODataClient(string uri)
        {
            var settings = new SimpleSettings();

            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream("TrekkingForCharity.Fenix.Client.metadata.xml"))
            {
                using (var reader = new StreamReader(stream))
                {
                    settings.MetadataDocument = reader.ReadToEnd();
                }
            }
            settings.UnqualifiedNameCall = false;
            settings.BaseUri = new Uri(uri);
            this._client = new SimpleClient(settings);
        }

        public BaseODataQuery<TQueryEntity> For<TQueryEntity>() where TQueryEntity : QueryEntity
        {
            return new ODataQuery<TQueryEntity>(this._client);
        } 
    }
}
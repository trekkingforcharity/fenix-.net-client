﻿using System;
namespace TrekkingForCharity.Fenix.Client.Queries.Entities.Waypoint 
{
	public sealed class Waypoint : QueryEntity
	{
		public String Name { get; set; }
		public Single Lat { get; set; }
		public Single Lng { get; set; }
		public Guid TrekId { get; set; }
		public Guid Id { get; set; }
	}
}
namespace TrekkingForCharity.Fenix.Client.Queries.Entities.User 
{
	public sealed class User : QueryEntity
	{
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public String EmailAddress { get; set; }
		public Boolean IsConfirmed { get; set; }
		public Guid Id { get; set; }
	}
}
namespace TrekkingForCharity.Fenix.Client.Queries.Entities.Update 
{
	public sealed class Update : QueryEntity
	{
		public String Description { get; set; }
		public Double Lng { get; set; }
		public Double Lat { get; set; }
		public Guid TrekId { get; set; }
		public Guid Id { get; set; }
	}
}
namespace TrekkingForCharity.Fenix.Client.Queries.Entities.Trek 
{
	public sealed class ArchivedTrek : QueryEntity
	{
		public String Name { get; set; }
		public String Slug { get; set; }
		public String Description { get; set; }
		public DateTime StartDate { get; set; }
		public Guid CreatedByUserId { get; set; }
		public DateTime WhenCreated { get; set; }
		public Int32 UpdateCount { get; set; }
		public Int32 WaypointCount { get; set; }
		public Boolean IsActive { get; set; }
		public Boolean IsPublished { get; set; }
		public Guid Id { get; set; }
	}
	public sealed class Trek : QueryEntity
	{
		public String Name { get; set; }
		public String Slug { get; set; }
		public String Description { get; set; }
		public DateTimeOffset StartDate { get; set; }
		public Guid CreatedByUserId { get; set; }
		public DateTime WhenCreated { get; set; }
		public Int32 UpdateCount { get; set; }
		public Int32 WaypointCount { get; set; }
		public Boolean IsActive { get; set; }
		public Boolean IsPublished { get; set; }
		public Guid Id { get; set; }
	}
}
namespace TrekkingForCharity.Fenix.Client.Queries.Entities.Auth 
{
	public sealed class AuthApplication : QueryEntity
	{
		public Guid UserId { get; set; }
		public String Name { get; set; }
		public DateTime WhenApplied { get; set; }
		public Guid ApiKey { get; set; }
		public String Hash { get; set; }
		public Boolean IsActive { get; set; }
		public Guid Id { get; set; }
	}
	public sealed class AuthToken : QueryEntity
	{
		public String Token { get; set; }
		public DateTime WhenExpires { get; set; }
		public DateTime WhenCreated { get; set; }
		public Guid UserId { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public String EmailAddress { get; set; }
		public Guid AuthApplicationId { get; set; }
		public Guid ApiKey { get; set; }
		public Guid Id { get; set; }
	}
}

﻿using System;
namespace TrekkingForCharity.Fenix.Client.GlobalConstants
{
	public enum ErrorCodes	{
	}

	[Flags]
	public enum TrekStatus	{
		Archived = 1, 
				Published = 2, 
				Active = 4, 
			}

}
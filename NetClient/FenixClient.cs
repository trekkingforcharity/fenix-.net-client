﻿namespace TrekkingForCharity.Fenix.Client
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Exceptions;
    using Executors.CommandResults;
    using Executors.CommandResults.Auth;
    using Executors.Commands;
    using Executors.Commands.Auth;
    using Infrastructure;
    using Newtonsoft.Json;
    using Polly;
    using Queries;
    using Queries.Entities;
    using RestSharp;
    using TrekkingForCharity.Fenix.Client.Queries.QueryGeneration;

    /// <summary>
    /// 
    /// </summary>
    public class FenixClient : IFenixClient
    {
        private readonly ODataClient _client;
        private readonly Policy _policy;
        private readonly AuthorizationProvider _provider;


        /// <summary>
        /// Initializes a new instance of the <see cref="FenixClient"/> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        public FenixClient(AuthorizationProvider provider)
        {
            _provider = provider;
            _client = new ODataClient(_provider.ApiPath);
            _policy = Policy
                .Handle<ServerNotAccessibleException>()
                .WaitAndRetryAsync(new[]
                {
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(2),
                    TimeSpan.FromSeconds(3)
                }, (exception, timeSpan, task) =>
                {
                    NotifyEvent?.Invoke(this, new NoticationEventArgs
                    {
                        Message = exception.ToString()
                    });
                });
        }


        /// <summary>
        /// Gets the API key.
        /// </summary>
        /// <value>
        /// The API key.
        /// </value>
        public Guid ApiKey => _provider.ApiKey;


        /// <summary>
        /// Creates the query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public BaseODataQuery<T> CreateQuery<T>() where T : QueryEntity
        {
            return _client.For<T>();
        }


        /// <summary>
        /// Occurs when [notify event].
        /// </summary>
        public event EventHandler<NoticationEventArgs> NotifyEvent;

        /// <summary>
        /// Queries the specified query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public async Task<ApiQueryResponse<T>> Query<T>(BaseODataQuery<T> query, bool executeAsPost = true) where T : QueryEntity
        {
            var odataQuery = query as ODataQuery<T>;
            var filterString = await odataQuery.GetCommandTextAsync();
            
            var filterSplit = filterString.Split('?');

            var client = executeAsPost
                ? new RestClient($"{_provider.ApiPath}{Guid.NewGuid()}/read/{filterSplit.First()}")
                : new RestClient($"{_provider.ApiPath}{Guid.NewGuid()}/read/{filterString}"); 

            var request = new RestRequest {RequestFormat = DataFormat.Json, Method = executeAsPost ? Method.POST : Method.GET};

            request.AddHeader("Authorization", $"Token {_provider.AuthKey}");

            if (executeAsPost && filterSplit.Length > 1)
                request.AddBody($"?{filterSplit.Last()}");

            var response = await _policy.ExecuteAsync(() => ExecuteRestRequest(client, request));

            return JsonConvert.DeserializeObject<ApiQueryResponse<T>>(response.Content);
        }

        /// <summary>
        /// Executes the specified command.
        /// </summary>
        /// <typeparam name="TCommand">The type of the command.</typeparam>
        /// <typeparam name="TCommandResult">The type of the command result.</typeparam>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public async Task<ExecutionResult<TCommandResult>> Execute<TCommand, TCommandResult>(TCommand command)
            where TCommand : BaseCommand
            where TCommandResult : BaseCommandResult
        {
            if (_provider.NewTokenNeeded())
            {
                await GetNewToken();
            }

            var client =
                new RestClient($"{_provider.ApiPath}{Guid.NewGuid()}/execute/{command.GetRoute()}");
            var request = new RestRequest {RequestFormat = DataFormat.Json, Method = Method.POST};
            request.AddHeader("Authorization", $"Token {_provider.AuthKey}");
            request.AddBody(command);


            var response = await _policy.ExecuteAsync(() => ExecuteRestRequest(client, request));

            var result = JsonConvert.DeserializeObject<ExecutionResult<TCommandResult>>(response.Content);
            return result;
        }


        private async Task<bool> GetNewToken()
        {
            var cmd = new RenewTokenCommand {Token = _provider.Token};
            var client = new RestClient($"{_provider.ApiPath}{Guid.NewGuid()}/execute/{cmd.GetRoute()}");

            var request = new RestRequest {RequestFormat = DataFormat.Json, Method = Method.POST};
            request.AddHeader("Authorization", $"Token {_provider.AuthKey}");
            request.AddBody(cmd);

            var response = await _policy.ExecuteAsync(() => ExecuteRestRequest(client, request));


            var result = JsonConvert.DeserializeObject<ExecutionResult<RenewTokenCommandResult>>(response.Content);
            if (result.Success)
            {
                _provider.ProcessRenewToken(result.Result);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Logins the user.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public async Task<bool> LoginUser(string emailAddress, string password)
        {
            var cmd = new LoginUserCommand
            {
                EmailAddress = emailAddress,
                Password = password
            };

            var client = new RestClient($"{_provider.ApiPath}{Guid.NewGuid()}/execute/{cmd.GetRoute()}");

            var request = new RestRequest {RequestFormat = DataFormat.Json, Method = Method.POST};
            request.AddHeader("Authorization", $"Token {_provider.AuthKey}");
            request.AddBody(cmd);
            var response = await _policy.ExecuteAsync(() => ExecuteRestRequest(client, request));


            var result =
                JsonConvert.DeserializeObject<ExecutionResult<LoginUserCommandResult>>(response.Content);
            if (!result.Success)
                return false;

            _provider.ProcessLogin(result.Result);
            return true;
        }

        private Exception HandleError(IRestResponse originalResponse)
        {
            switch (originalResponse.StatusCode)
            {
                case 0:
                case HttpStatusCode.RequestTimeout:
                case HttpStatusCode.ServiceUnavailable:
                    return new ServerNotAccessibleException();
                case HttpStatusCode.Unauthorized:
                    return new UnauthorizedException();
                case HttpStatusCode.Forbidden:
                    return new NoAccessException();
                default:
                    return new Exception($"Error Message: {originalResponse.ErrorMessage}\r\nContent: {originalResponse.Content}");
            }
        }

        private async Task<IRestResponse> ExecuteRestRequest(IRestClient restClient, IRestRequest restRequest)
        {
            var response = await restClient.ExecuteTaskAsync(restRequest);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.BadRequest)
                return response;

            throw HandleError(response);
        }
    }
}
﻿namespace TrekkingForCharity.Fenix.Client.Infrastructure
{
    using System;

    public class NoticationEventArgs : EventArgs
    {
        public string Message { get; set; }
    }
}
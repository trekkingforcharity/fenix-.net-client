﻿namespace TrekkingForCharity.Fenix.Client.Exceptions
{
    using System;

    /// <summary>
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class NoAccessException : Exception
    {
    }
}
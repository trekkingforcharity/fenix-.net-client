﻿namespace TrekkingForCharity.Fenix.Client
{
    using System;
    using System.Threading.Tasks;
    using TrekkingForCharity.Fenix.Client.Executors.CommandResults;
    using TrekkingForCharity.Fenix.Client.Executors.Commands;
    using TrekkingForCharity.Fenix.Client.Infrastructure;
    using TrekkingForCharity.Fenix.Client.Queries;
    using TrekkingForCharity.Fenix.Client.Queries.Entities;
    using TrekkingForCharity.Fenix.Client.Queries.QueryGeneration;

    public interface IFenixClient
    {
        /// <summary>
        /// Gets the API key.
        /// </summary>
        /// <value>
        /// The API key.
        /// </value>
        Guid ApiKey { get; }

        /// <summary>
        /// Creates the query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        BaseODataQuery<T> CreateQuery<T>() where T : QueryEntity;

        /// <summary>
        /// Occurs when [notify event].
        /// </summary>
        event EventHandler<NoticationEventArgs> NotifyEvent;

        /// <summary>
        /// Queries the specified query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        Task<ApiQueryResponse<T>> Query<T>(BaseODataQuery<T> query, bool executeAsPost = true) where T : QueryEntity;

        /// <summary>
        /// Executes the specified command.
        /// </summary>
        /// <typeparam name="TCommand">The type of the command.</typeparam>
        /// <typeparam name="TCommandResult">The type of the command result.</typeparam>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        Task<ExecutionResult<TCommandResult>> Execute<TCommand, TCommandResult>(TCommand command)
            where TCommand : BaseCommand
            where TCommandResult : BaseCommandResult;

        /// <summary>
        /// Logins the user.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        Task<bool> LoginUser(string emailAddress, string password);
    }
}
﻿namespace TrekkingForCharity.Fenix.Client
{
    using System;
    
    public class FenixApiConfig : IFenixApiConfig
    {
        public Guid ApiKey { get; set; }
        public string ApiPath { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using TrekkingForCharity.Fenix.Client.GlobalConstants;
namespace TrekkingForCharity.Fenix.Client.Executors.Commands.Waypoint 
{
	/// <summary>
    /// The Add Waypoint To Trek Command 
    /// </summary>
	public sealed class AddWaypointToTrekCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="AddWaypointToTrekCommand"/> class.
        /// </summary>
		public AddWaypointToTrekCommand() : base("Waypoint/AddWaypointToTrekCommand") { }
		/// <summary>
        /// Gets or sets the Position 
        /// </summary>
        /// <value>
        /// The Position.
        /// </value>
		public Int32  Position { get; set; }

		/// <summary>
        /// Gets or sets the Lng 
        /// </summary>
        /// <value>
        /// The Lng.
        /// </value>
		public Double  Lng { get; set; }

		/// <summary>
        /// Gets or sets the Lat 
        /// </summary>
        /// <value>
        /// The Lat.
        /// </value>
		public Double  Lat { get; set; }

		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid  TrekId { get; set; }

		/// <summary>
        /// Gets or sets the Name 
        /// </summary>
        /// <value>
        /// The Name.
        /// </value>
		public String  Name { get; set; }

	}
		/// <summary>
    /// The Order Waypoints Command 
    /// </summary>
	public sealed class OrderWaypointsCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="OrderWaypointsCommand"/> class.
        /// </summary>
		public OrderWaypointsCommand() : base("Waypoint/OrderWaypointsCommand") { }
		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid  TrekId { get; set; }

		/// <summary>
        /// Gets or sets the New Order 
        /// </summary>
        /// <value>
        /// The New Order.
        /// </value>
		public IEnumerable<Guid>  NewOrder { get; set; }

	}
		/// <summary>
    /// The Remove Waypoint From Trek Command 
    /// </summary>
	public sealed class RemoveWaypointFromTrekCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="RemoveWaypointFromTrekCommand"/> class.
        /// </summary>
		public RemoveWaypointFromTrekCommand() : base("Waypoint/RemoveWaypointFromTrekCommand") { }
		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid  TrekId { get; set; }

		/// <summary>
        /// Gets or sets the Waypoint Id 
        /// </summary>
        /// <value>
        /// The Waypoint Id.
        /// </value>
		public Guid  WaypointId { get; set; }

	}
	}
namespace TrekkingForCharity.Fenix.Client.Executors.Commands.User 
{
	/// <summary>
    /// The Change Basic Details Command 
    /// </summary>
	public sealed class ChangeBasicDetailsCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="ChangeBasicDetailsCommand"/> class.
        /// </summary>
		public ChangeBasicDetailsCommand() : base("User/ChangeBasicDetailsCommand") { }
		/// <summary>
        /// Gets or sets the First Name 
        /// </summary>
        /// <value>
        /// The First Name.
        /// </value>
		public String  FirstName { get; set; }

		/// <summary>
        /// Gets or sets the Last Name 
        /// </summary>
        /// <value>
        /// The Last Name.
        /// </value>
		public String  LastName { get; set; }

	}
		/// <summary>
    /// The Change Email Command 
    /// </summary>
	public sealed class ChangeEmailCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="ChangeEmailCommand"/> class.
        /// </summary>
		public ChangeEmailCommand() : base("User/ChangeEmailCommand") { }
		/// <summary>
        /// Gets or sets the Old Email Address 
        /// </summary>
        /// <value>
        /// The Old Email Address.
        /// </value>
		public String  OldEmailAddress { get; set; }

		/// <summary>
        /// Gets or sets the New Email Address 
        /// </summary>
        /// <value>
        /// The New Email Address.
        /// </value>
		public String  NewEmailAddress { get; set; }

	}
		/// <summary>
    /// The Change Password Command 
    /// </summary>
	public sealed class ChangePasswordCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="ChangePasswordCommand"/> class.
        /// </summary>
		public ChangePasswordCommand() : base("User/ChangePasswordCommand") { }
		/// <summary>
        /// Gets or sets the Old Password 
        /// </summary>
        /// <value>
        /// The Old Password.
        /// </value>
		public String  OldPassword { get; set; }

		/// <summary>
        /// Gets or sets the New Password 
        /// </summary>
        /// <value>
        /// The New Password.
        /// </value>
		public String  NewPassword { get; set; }

	}
		/// <summary>
    /// The Create User Command 
    /// </summary>
	public sealed class CreateUserCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="CreateUserCommand"/> class.
        /// </summary>
		public CreateUserCommand() : base("User/CreateUserCommand") { }
		/// <summary>
        /// Gets or sets the First Name 
        /// </summary>
        /// <value>
        /// The First Name.
        /// </value>
		public String  FirstName { get; set; }

		/// <summary>
        /// Gets or sets the Last Name 
        /// </summary>
        /// <value>
        /// The Last Name.
        /// </value>
		public String  LastName { get; set; }

		/// <summary>
        /// Gets or sets the Email Address 
        /// </summary>
        /// <value>
        /// The Email Address.
        /// </value>
		public String  EmailAddress { get; set; }

		/// <summary>
        /// Gets or sets the Password 
        /// </summary>
        /// <value>
        /// The Password.
        /// </value>
		public String  Password { get; set; }

	}
		/// <summary>
    /// The Reset Password Command 
    /// </summary>
	public sealed class ResetPasswordCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="ResetPasswordCommand"/> class.
        /// </summary>
		public ResetPasswordCommand() : base("User/ResetPasswordCommand") { }
		/// <summary>
        /// Gets or sets the Reset Token 
        /// </summary>
        /// <value>
        /// The Reset Token.
        /// </value>
		public String  ResetToken { get; set; }

		/// <summary>
        /// Gets or sets the Password 
        /// </summary>
        /// <value>
        /// The Password.
        /// </value>
		public String  Password { get; set; }

	}
		/// <summary>
    /// The Send Password Reset Token Command 
    /// </summary>
	public sealed class SendPasswordResetTokenCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="SendPasswordResetTokenCommand"/> class.
        /// </summary>
		public SendPasswordResetTokenCommand() : base("User/SendPasswordResetTokenCommand") { }
		/// <summary>
        /// Gets or sets the Email Address 
        /// </summary>
        /// <value>
        /// The Email Address.
        /// </value>
		public String  EmailAddress { get; set; }

	}
	}
namespace TrekkingForCharity.Fenix.Client.Executors.Commands.Update 
{
	/// <summary>
    /// The Add Update To Trek Command 
    /// </summary>
	public sealed class AddUpdateToTrekCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="AddUpdateToTrekCommand"/> class.
        /// </summary>
		public AddUpdateToTrekCommand() : base("Update/AddUpdateToTrekCommand") { }
		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid  TrekId { get; set; }

		/// <summary>
        /// Gets or sets the Name 
        /// </summary>
        /// <value>
        /// The Name.
        /// </value>
		public String  Name { get; set; }

		/// <summary>
        /// Gets or sets the Description 
        /// </summary>
        /// <value>
        /// The Description.
        /// </value>
		public String  Description { get; set; }

		/// <summary>
        /// Gets or sets the Lng 
        /// </summary>
        /// <value>
        /// The Lng.
        /// </value>
		public Double  Lng { get; set; }

		/// <summary>
        /// Gets or sets the Lat 
        /// </summary>
        /// <value>
        /// The Lat.
        /// </value>
		public Double  Lat { get; set; }

		/// <summary>
        /// Gets or sets the Cover Photo File Name 
        /// </summary>
        /// <value>
        /// The Cover Photo File Name.
        /// </value>
		public String  CoverPhotoFileName { get; set; }

		/// <summary>
        /// Gets or sets the Cover Photo Content Compressed 
        /// </summary>
        /// <value>
        /// The Cover Photo Content Compressed.
        /// </value>
		public String  CoverPhotoContentCompressed { get; set; }

	}
	}
namespace TrekkingForCharity.Fenix.Client.Executors.Commands.Trek 
{
	/// <summary>
    /// The Activate Trek Command 
    /// </summary>
	public sealed class ActivateTrekCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="ActivateTrekCommand"/> class.
        /// </summary>
		public ActivateTrekCommand() : base("Trek/ActivateTrekCommand") { }
		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid  TrekId { get; set; }

	}
		/// <summary>
    /// The Change Basic Details Command 
    /// </summary>
	public sealed class ChangeBasicDetailsCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="ChangeBasicDetailsCommand"/> class.
        /// </summary>
		public ChangeBasicDetailsCommand() : base("Trek/ChangeBasicDetailsCommand") { }
		/// <summary>
        /// Gets or sets the Description 
        /// </summary>
        /// <value>
        /// The Description.
        /// </value>
		public String  Description { get; set; }

		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid  TrekId { get; set; }

	}
		/// <summary>
    /// The Change Trek Name Command 
    /// </summary>
	public sealed class ChangeTrekNameCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="ChangeTrekNameCommand"/> class.
        /// </summary>
		public ChangeTrekNameCommand() : base("Trek/ChangeTrekNameCommand") { }
		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid  TrekId { get; set; }

		/// <summary>
        /// Gets or sets the Name 
        /// </summary>
        /// <value>
        /// The Name.
        /// </value>
		public String  Name { get; set; }

	}
		/// <summary>
    /// The Create Trek Command 
    /// </summary>
	public sealed class CreateTrekCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="CreateTrekCommand"/> class.
        /// </summary>
		public CreateTrekCommand() : base("Trek/CreateTrekCommand") { }
		/// <summary>
        /// Gets or sets the Name 
        /// </summary>
        /// <value>
        /// The Name.
        /// </value>
		public String  Name { get; set; }

		/// <summary>
        /// Gets or sets the Description 
        /// </summary>
        /// <value>
        /// The Description.
        /// </value>
		public String  Description { get; set; }

		/// <summary>
        /// Gets or sets the Cover Photo File Name 
        /// </summary>
        /// <value>
        /// The Cover Photo File Name.
        /// </value>
		public String  CoverPhotoFileName { get; set; }

		/// <summary>
        /// Gets or sets the Cover Photo Content Compressed 
        /// </summary>
        /// <value>
        /// The Cover Photo Content Compressed.
        /// </value>
		public String  CoverPhotoContentCompressed { get; set; }

	}
		/// <summary>
    /// The Deactivate Trek Command 
    /// </summary>
	public sealed class DeactivateTrekCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="DeactivateTrekCommand"/> class.
        /// </summary>
		public DeactivateTrekCommand() : base("Trek/DeactivateTrekCommand") { }
		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid  TrekId { get; set; }

	}
	}
namespace TrekkingForCharity.Fenix.Client.Executors.Commands.Auth 
{
	/// <summary>
    /// The Create Auth Application Command 
    /// </summary>
	public sealed class CreateAuthApplicationCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="CreateAuthApplicationCommand"/> class.
        /// </summary>
		public CreateAuthApplicationCommand() : base("Auth/CreateAuthApplicationCommand") { }
		/// <summary>
        /// Gets or sets the User Id 
        /// </summary>
        /// <value>
        /// The User Id.
        /// </value>
		public Guid  UserId { get; set; }

		/// <summary>
        /// Gets or sets the Name 
        /// </summary>
        /// <value>
        /// The Name.
        /// </value>
		public String  Name { get; set; }

	}
		/// <summary>
    /// The Login User Command 
    /// </summary>
	public sealed class LoginUserCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="LoginUserCommand"/> class.
        /// </summary>
		public LoginUserCommand() : base("Auth/LoginUserCommand") { }
		/// <summary>
        /// Gets or sets the Email Address 
        /// </summary>
        /// <value>
        /// The Email Address.
        /// </value>
		public String  EmailAddress { get; set; }

		/// <summary>
        /// Gets or sets the Password 
        /// </summary>
        /// <value>
        /// The Password.
        /// </value>
		public String  Password { get; set; }

	}
		/// <summary>
    /// The Renew Token Command 
    /// </summary>
	public sealed class RenewTokenCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="RenewTokenCommand"/> class.
        /// </summary>
		public RenewTokenCommand() : base("Auth/RenewTokenCommand") { }
		/// <summary>
        /// Gets or sets the Token 
        /// </summary>
        /// <value>
        /// The Token.
        /// </value>
		public String  Token { get; set; }

	}
		/// <summary>
    /// The Verify User Command 
    /// </summary>
	public sealed class VerifyUserCommand : BaseCommand
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="VerifyUserCommand"/> class.
        /// </summary>
		public VerifyUserCommand() : base("Auth/VerifyUserCommand") { }
		/// <summary>
        /// Gets or sets the Confirmation Token 
        /// </summary>
        /// <value>
        /// The Confirmation Token.
        /// </value>
		public String  ConfirmationToken { get; set; }

	}
	}

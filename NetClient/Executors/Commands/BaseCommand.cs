﻿namespace TrekkingForCharity.Fenix.Client.Executors.Commands
{
    public abstract class BaseCommand
    {
        /// <summary>
        ///     Gets the route.
        /// </summary>
        private readonly string route;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseCommand" /> class.
        /// </summary>
        /// <param name="route">The route.</param>
        protected BaseCommand(string route)
        {
            this.route = route;
        }

        /// <summary>
        ///     The get route.
        /// </summary>
        /// <returns>
        ///     The <see cref="System.String" /> .
        /// </returns>
        internal string GetRoute()
        {
            return this.route;
        }
    }
}

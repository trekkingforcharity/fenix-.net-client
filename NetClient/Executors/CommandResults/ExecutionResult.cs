﻿namespace TrekkingForCharity.Fenix.Client.Executors.CommandResults
{
    using System.Collections.Generic;

    public class ExecutionResult<T>
        where T : BaseCommandResult
    {
        /// <summary>
        ///     Gets or sets the result.
        /// </summary>
        /// <value>
        ///     The result.
        /// </value>
        public T Result { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether success.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        ///     Gets or sets the fail message.
        /// </summary>
        public string FailMessage { get; set; }

        /// <summary>
        ///     Gets or sets the errors.
        /// </summary>
        public ICollection<DomainError> Errors { get; set; }

    }
}

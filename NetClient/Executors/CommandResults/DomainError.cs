﻿namespace TrekkingForCharity.Fenix.Client.Executors.CommandResults
{
    using TrekkingForCharity.Fenix.Client.GlobalConstants;

    public class DomainError
    {
        public string Property { get; set; }
        public ErrorCodes ErrorCode { get; set; }
        public string Message { get; set; }
    }
}
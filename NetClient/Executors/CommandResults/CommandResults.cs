﻿using System;
using System.Collections.Generic;
using TrekkingForCharity.Fenix.Client.GlobalConstants;
using TrekkingForCharity.Fenix.Client.Executors.CommandResults;
namespace TrekkingForCharity.Fenix.Client.Executors.CommandResults.Waypoint 
{
	/// <summary>
    /// The Add Waypoint To Trek Command Result 
    /// </summary>
	public sealed class AddWaypointToTrekCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Order Waypoints Command Result 
    /// </summary>
	public sealed class OrderWaypointsCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Remove Waypoint From Trek Command Result 
    /// </summary>
	public sealed class RemoveWaypointFromTrekCommandResult : BaseCommandResult
	{
	}

}
namespace TrekkingForCharity.Fenix.Client.Executors.CommandResults.User 
{
	/// <summary>
    /// The Change Basic Details Command Result 
    /// </summary>
	public sealed class ChangeBasicDetailsCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Change Email Command Result 
    /// </summary>
	public sealed class ChangeEmailCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Change Password Command Result 
    /// </summary>
	public sealed class ChangePasswordCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Create User Command Result 
    /// </summary>
	public sealed class CreateUserCommandResult : BaseCommandResult
	{
		/// <summary>
        /// Gets or sets the User Id 
        /// </summary>
        /// <value>
        /// The User Id.
        /// </value>
		public Guid UserId { get; set; }
	}

	/// <summary>
    /// The Reset Password Command Result 
    /// </summary>
	public sealed class ResetPasswordCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Send Password Reset Token Command Result 
    /// </summary>
	public sealed class SendPasswordResetTokenCommandResult : BaseCommandResult
	{
		/// <summary>
        /// Gets or sets the Token 
        /// </summary>
        /// <value>
        /// The Token.
        /// </value>
		public String Token { get; set; }
	}

}
namespace TrekkingForCharity.Fenix.Client.Executors.CommandResults.Update 
{
	/// <summary>
    /// The Add Update To Trek Command Result 
    /// </summary>
	public sealed class AddUpdateToTrekCommandResult : BaseCommandResult
	{
		/// <summary>
        /// Gets or sets the Update Id 
        /// </summary>
        /// <value>
        /// The Update Id.
        /// </value>
		public Guid UpdateId { get; set; }
	}

}
namespace TrekkingForCharity.Fenix.Client.Executors.CommandResults.Trek 
{
	/// <summary>
    /// The Activate Trek Command Result 
    /// </summary>
	public sealed class ActivateTrekCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Change Basic Details Command Result 
    /// </summary>
	public sealed class ChangeBasicDetailsCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Change Trek Name Command Result 
    /// </summary>
	public sealed class ChangeTrekNameCommandResult : BaseCommandResult
	{
	}

	/// <summary>
    /// The Create Trek Command Result 
    /// </summary>
	public sealed class CreateTrekCommandResult : BaseCommandResult
	{
		/// <summary>
        /// Gets or sets the Trek Id 
        /// </summary>
        /// <value>
        /// The Trek Id.
        /// </value>
		public Guid TrekId { get; set; }
	}

	/// <summary>
    /// The Deactivate Trek Command Result 
    /// </summary>
	public sealed class DeactivateTrekCommandResult : BaseCommandResult
	{
	}

}
namespace TrekkingForCharity.Fenix.Client.Executors.CommandResults.Auth 
{
	/// <summary>
    /// The Create Auth Application Command Result 
    /// </summary>
	public sealed class CreateAuthApplicationCommandResult : BaseCommandResult
	{
		/// <summary>
        /// Gets or sets the Api Key 
        /// </summary>
        /// <value>
        /// The Api Key.
        /// </value>
		public Guid ApiKey { get; set; }
	}

	/// <summary>
    /// The Login User Command Result 
    /// </summary>
	public sealed class LoginUserCommandResult : BaseCommandResult
	{
		/// <summary>
        /// Gets or sets the User Id 
        /// </summary>
        /// <value>
        /// The User Id.
        /// </value>
		public Guid UserId { get; set; }
		/// <summary>
        /// Gets or sets the Auth Token 
        /// </summary>
        /// <value>
        /// The Auth Token.
        /// </value>
		public String AuthToken { get; set; }
		/// <summary>
        /// Gets or sets the Expire Date 
        /// </summary>
        /// <value>
        /// The Expire Date.
        /// </value>
		public DateTime ExpireDate { get; set; }
		/// <summary>
        /// Gets or sets the Email Address 
        /// </summary>
        /// <value>
        /// The Email Address.
        /// </value>
		public String EmailAddress { get; set; }
		/// <summary>
        /// Gets or sets the First Name 
        /// </summary>
        /// <value>
        /// The First Name.
        /// </value>
		public String FirstName { get; set; }
		/// <summary>
        /// Gets or sets the Last Name 
        /// </summary>
        /// <value>
        /// The Last Name.
        /// </value>
		public String LastName { get; set; }
	}

	/// <summary>
    /// The Renew Token Command Result 
    /// </summary>
	public sealed class RenewTokenCommandResult : BaseCommandResult
	{
		/// <summary>
        /// Gets or sets the User Id 
        /// </summary>
        /// <value>
        /// The User Id.
        /// </value>
		public Guid UserId { get; set; }
		/// <summary>
        /// Gets or sets the Auth Token 
        /// </summary>
        /// <value>
        /// The Auth Token.
        /// </value>
		public String AuthToken { get; set; }
		/// <summary>
        /// Gets or sets the Expire Date 
        /// </summary>
        /// <value>
        /// The Expire Date.
        /// </value>
		public DateTime ExpireDate { get; set; }
	}

	/// <summary>
    /// The Verify User Command Result 
    /// </summary>
	public sealed class VerifyUserCommandResult : BaseCommandResult
	{
	}

}

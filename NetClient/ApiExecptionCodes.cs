﻿namespace TrekkingForCharity.Fenix.Client
{
    public enum ApiExecptionCodes
    {
        /// <summary>
        /// The no creditials.
        /// </summary>
        NoCreditials = 100,

        /// <summary>
        /// The invalid creditials.
        /// </summary>
        InvalidCreditials = 200,

        /// <summary>
        /// The no access.
        /// </summary>
        NoAccess = 300,

        /// <summary>
        /// The error not typed.
        /// </summary>
        ErrorNotTyped = 400,
        ServerNotAvaliable
    }
}

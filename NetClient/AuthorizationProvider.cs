﻿namespace TrekkingForCharity.Fenix.Client
{
    using System;
    using System.Configuration;
    using System.Text;
    using Newtonsoft.Json;
    using TrekkingForCharity.Fenix.Client.Executors.CommandResults;
    using TrekkingForCharity.Fenix.Client.Executors.CommandResults.Auth;

    public abstract class AuthorizationProvider
    {
        /// <summary>
        ///     The _api config.
        /// </summary>
        private readonly IFenixApiConfig _apiConfig;

        protected AuthorizationProvider(string apiPath, Guid apiKey)
        {
            this._apiConfig = new FenixApiConfig { ApiKey = apiKey, ApiPath = apiPath };
        }

        /// <summary>
        ///     Gets or sets the token.
        /// </summary>
        public abstract string Token { get; set; }

        /// <summary>
        ///     Gets or sets the expiry date.
        /// </summary>
        public abstract DateTime? ExpiryDate { get; set; }

        /// <summary>
        ///     Gets the auth key.
        /// </summary>
        public string AuthKey
        {
            get
            {
                string authkey;
                if (this.ExpiryDate.HasValue && this.ExpiryDate.Value > DateTime.UtcNow)
                    authkey = JsonConvert.SerializeObject(new { this._apiConfig.ApiKey, this.Token });
                else
                {
                    authkey = JsonConvert.SerializeObject(new { ApiKey = this._apiConfig.ApiKey, Token = string.Empty });
                }
                var plainTextBytes = Encoding.UTF8.GetBytes(authkey);
                return Convert.ToBase64String(plainTextBytes);
            }
        }

        public string ApiPath => _apiConfig.ApiPath;
        public Guid ApiKey => _apiConfig.ApiKey;


        public bool NewTokenNeeded()
        {
            if (string.IsNullOrWhiteSpace(this.Token))
                return false;
            if (this.ExpiryDate.HasValue && this.ExpiryDate.Value > DateTime.UtcNow)
                return false;

            return true;
        }

        public abstract void ProcessRenewToken(RenewTokenCommandResult renewTokenCommandResult);
        public abstract void ProcessLogin(LoginUserCommandResult loginUserCommandResult);
    }
}

﻿namespace TrekkingForCharity.Fenix.Client
{
    using System;

    public interface IFenixApiConfig
    {
        Guid ApiKey { get; set; }
        string ApiPath { get; set; }
    }
}

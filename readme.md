﻿# Fenix .Net client #
> .Net assembly to connect to Trekking for Charity's 'Fenix' Api.  Built against Fenix 1.0.0.0
## Getting Started ##

This component is hosted on a public feed at [myget.org](https://www.myget.org/feed/trekking-for-charity-nuget/package/nuget/TrekkingForCharity.Fenix.Client)  and to register the feed you can follow these [instructions](http://docs.myget.org/docs/walkthrough/getting-started-with-nuget).

Install the package:

	PM> Install-Package TrekkingForCharity.Fenix.Client